from telethon.sync import TelegramClient, events
from dotenv import load_dotenv
import re
import os

load_dotenv(override=True)

api_id = os.getenv("API_ID") #Personal API
api_hash = os.getenv("API_HASH") #Personal API
phone = os.getenv("PHONE") #Phone Number used for Personal API
user_name = os.getenv("USER_NAME") #Personal User Name
bot_token = os.getenv("BOT_TOKEN") #Bot API token

patterns = [r'\bGO\b', r'\bSLOT', r'\bAVAILABLE\b', r'\bAVAILABILITY\b', r'\bOPEN', r'\bBOOK']

GROUP_MUMBAI_VISA_STAMPING = -1001277594167
GROUP_H1_H4_DROPBOX_VISA_SLOTS = -1001776493611
GROUP_H4BDROPBOX = -1001699265531
GROUP_H1B_H4_VISA_DROPBOX_SLOTS = -1001371184682
GROUP_DROPBOX_VISA_SLOTS_FOR_ALL_VISA_CATEGORIES = -1001321004285
GROUP_B1B2SLOTAVAILABILITY = -1001677810942

with TelegramClient(user_name, api_id, api_hash).start(phone=phone) as client:
    with TelegramClient('bot',api_id,api_hash).start(bot_token=bot_token) as bot:

        @bot.on(events.NewMessage()) #On Subscribe to Bot / On MessageBot
        async def handler(event):
            sender = await event.get_sender()
            print(sender.username)
            print(sender.id)
            await notify(event, '@RajTestBot!')

        @client.on(events.NewMessage(GROUP_MUMBAI_VISA_STAMPING))
        async def handler(event):
            await notify(event, 'https://t.me/Mumbai_Visa_Stamping!')

        @client.on(events.NewMessage(GROUP_H1_H4_DROPBOX_VISA_SLOTS))
        async def handler(event):
            await notify(event, 'https://t.me/H1_H4_dropbox_visa_slots!')

        @client.on(events.NewMessage(GROUP_H4BDROPBOX))
        async def handler(event):
            await notify(event, 'https://t.me/H4Bdropbox!')

        @client.on(events.NewMessage(GROUP_H1B_H4_VISA_DROPBOX_SLOTS))
        async def handler(event):
            await notify(event, 'https://t.me/H1B_H4_Visa_Dropbox_slots!')

        @client.on(events.NewMessage(GROUP_DROPBOX_VISA_SLOTS_FOR_ALL_VISA_CATEGORIES))
        async def handler(event):
            await notify(event, 'https://t.me/joinchat/U7ZEg0687P2elB9-_er55Q!')

        @client.on(events.NewMessage(GROUP_B1B2SLOTAVAILABILITY))
        async def handler(event):
            await notify(event, 'https://t.me/B1B2SlotAvailability!')

        async def notify(event, group):
            participants = [participant.id for participant in await client.get_participants(user_name)]
            me = participants[0]
            for pattern in patterns:
                if re.search(pattern, event.raw_text, re.IGNORECASE): # if text contains a pattern
                    sender = await event.get_sender()
                    sender_name = sender.username
                    msg = "GROUP: " + group + " FROM: " + sender_name + " WORD: " + pattern
                    await bot.send_message(me, msg)
                    break
            
        client.run_until_disconnected()
