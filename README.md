# Visa Appointment Notifier

Get notified about Visa Appointment Slots from different Telegram Groups.

## Initial Setup
- Create API ID and API Hash from [here](https://my.telegram.org/).
- Create a bot and get the access token via [BotFather](https://core.telegram.org/bots#6-botfather).


## Install

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install.

```
$ python3 -m venv ./venv
$ source venv/bin/activate
$ pip install -r requirements.txt
```

## Config Setup

Create an .env file with the following configuration:

```
API_ID=<YOUR_API_ID>
API_HASH='<YOUR_API_HASH>'
PHONE='<YOUR_PHONE_NUMBER>'
USER_NAME='<YOUR_USER_NAME>'
BOT_TOKEN='<YOUR_BOT_TOKEN>'
```

## Run

```
$ python3 getChats.py
```

## Alternative: Run as a background process
- Skip Install step
- Give permission to execute the bash script:
```
$ chmod +x visa_finder.sh
```
- Run:
```
$ ./visa_finder.sh &
```
- Figure out the pid of ```python3 getChats.py``` with ```$ ps -ef```
- Pass the telegram code for sign-in:
```
$ echo "<code>" > /proc/<pid>/fd/0
```


## Contribute

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)
